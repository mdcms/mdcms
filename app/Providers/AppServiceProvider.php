<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerVendors();
	}

	/**
	 * Регистрация вендоров
	 */
	protected function registerVendors()
	{
		$loader = \Illuminate\Foundation\AliasLoader::getInstance();

		$this->app->register('Nwidart\Modules\LaravelModulesServiceProvider');
		$this->app->register('Magnusdeus\Themes\ThemesServiceProvider');

		$loader->alias('Module', 'Nwidart\Modules\Facades\Module');
		$loader->alias('Theme', 'Magnusdeus\Themes\ThemeFacade');

		if ($this->app->environment() !== 'production') {
			$this->app->register('Barryvdh\Debugbar\ServiceProvider');
			$this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
			$loader->alias('Debugbar', 'Barryvdh\Debugbar\Facade');
		}
	}

}
