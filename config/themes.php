<?php

return [

    'default' => 'default',

    'path' => public_path('themes'),

    'cache' => [
        'enabled' => false,
        'key' => 'pingpong.themes',
        'lifetime' => 86400,
    ],

];
